
import outline from './section/outline.js';
import demandAnalysls from './section/demandAnalysls.js';
import technologyStack from './section/technologyStack.js';
import projectConstruct from './section/projectConstruct.js';
import codeStyle from './section/codeStyle.js';
import problem from './section/problem.js';
import showProject from './section/showProject.js';
import optimize from './section/optimize.js';
import summary from './section/summary.js';
import end from './section/end.js';

//const
//包裹 section 的dom
const slideDom = document.querySelector('.slide-hook');

//register section
const slideSections = [
    outline,
    showProject,
    demandAnalysls,
    technologyStack,
    projectConstruct,
    codeStyle,
    problem,
    optimize,
    summary,
    end
]

//入口函数
const main = () => {
    addSection();
}

//add per section in index
const addSection = () =>{
    slideSections.forEach((item, index) =>{
        slideDom.insertAdjacentHTML('beforebegin', item);
    })
}

main();



