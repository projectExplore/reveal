
let section =
`<section class="section-summary">
        <div class="summary-title">
            项目展示
        </div>
        <div class="summary-title">
            原始需求分析
        </div>
        <div class="summary-title">
            使用的技术栈 + 工具
        </div>
        <div class="summary-title">
            工程结构
        </div>
         <div class="summary-title">
            编码风格
        </div>
        <div class="summary-title">
            遇到的问题
        </div>
        <div class="summary-title">
            优化
        </div>
        <div class="summary-title">
            总结
        </div>
</section>
`

export default section;

