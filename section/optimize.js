
let section =
`<section class="project-construct">
    <section>
        <h2 class="title">
           优化
        </h2> 
        <div class="fragment">
           <div>轮询</div>
           <div>服务端返回错误</div>
           <div>组件优化</div>
        </div>
    </section>
    
    <section>
        <div class>
            <h2 class="title">
                轮询
            </h2>
        </div>
        <div class="fragment">
            <div>es5: callback </div>
            <div>传入callback</div>
            <div>请求结果运行callback</div>
            <div>回调函数中判断轮询结果...do something </div>
        </div>
    </section>
    
    <section>
        <div class>
            <h2 class="title">
                轮询
            </h2>
        </div>
        <div>es6: Promise </div>
        <div class="fragment">
            <pre>
                //action
                return new Promise((resolve, reject) => {
                    result ? resolve(res) : reject(err)
                });
                
                //component
                promise.then(function(value) {
                    // success do something
                }, function(error) {
                    // failure
                });
            </pre>
        </div>
        <div class="fragment">
            <div>Generator</div>
            <div>yield、next</div>
        </div>
    </section>
    
    <section>
        <div class>
            <h2 class="title">
                轮询
            </h2>
        </div>
        <div>es7(ES2017) async await</div>
        <div class="fragment">
            <pre>
                //action
                return fetch(api)
                        .then(resolve(res))
            
                // component    
                let res = await this.props.pollingQuery(sendParam);
                res.result.finished ? A : B;
            </pre>
        </div>
    </section>
    
    <section>
        <div class>
            <h2 class="title">
                服务端返回错误
            </h2>
            <div class="fragment">
                请求出错，提示用户报错，并3s后再次请求
            </div>
        </div>
    </section>
    <section>
        <div class="img-wrap">
            <img src="./img/request.gif" alt="">
        </div>
    </section>
    
    <section>
        <div>
            <h2 class="title">
                组件优化
            </h2>    
        </div>
    </section>
    <section>
        <div>
            <div class="img-wrap">
                <img src="./img/optimize.gif" alt="">
            </div>
        </div>
    </section>
    <section>
        <div>去除 css</div>
        <div>create-react-app 含 scss-loader</div>
        <div>import scss</div>
    </section>
    
</section>
`

export default section;
