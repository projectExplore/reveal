
let section =
    `<section class="code-style">
    <section>
        <div>
            <h2 class="title">
              代码规范
            </h2>
        </div>
    </section>
    <section class="code-style-detail">
        <div class="item">
            1：代码千万行,注释第一行</div>
        </div>
        <div class="item fragment">
            2：使用合适的变量名、方法名(Camel-Case)
        </div>
        <div class="item fragment">
            <div>3：commit/push 注释清晰</div>
            <div class="ti1-5">@dev update/fix/add/remove ...</div>  
        </div>
        <div class="fragment">
            4：团队协作开发，.editorconfig 保持一致
        </div>
        
    </section>
    
    <section>
        <div class="img-wrap">
            <img src="./img/codeStyle1.jpg" alt="">
        </div>
    </section>
    
    <section>
        <div class="img-wrap">
            <img src="./img/codeStyle2.jpg" alt="">
        </div>
    </section>
    
    <section>
        <div class="img-wrap">
            <img src="./img/codeStyle.jpg" alt="">
        </div>
    </section>
</section>
`

export default section;
