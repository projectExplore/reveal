
let section =
    `<section class="demand-tec">
    <section>
        <div>
            <h2 class="title">
              使用的技术栈 + 工具
            </h2>
        </div>
    </section>
    <section class="demand-detail">
        <div class="item">
           1、typescript 开发
        </div>
        <div class="item fragment">
           2、React全家桶: 前端框架 + 路由
        </div>
        <div class="item fragment">
           3、Redux: 前端的状态管理
        </div>
        <div class="item fragment">
           4、material UI (extends jss): 前端 UI 组件库
        </div>
        <div class="item fragment">
           5、SCSS （gulp watch scss -> css ）: 组件样式
        </div>
        <div class="item fragment">
           6、fetch 请求服务端数据
        </div>
        <div class="item fragment">
           7、jszip、file-saver 下载zip包功能
        </div>
        <div class="item fragment">
           8、moment：时间戳 format
        </div>
    </section>
</section>
`

export default section;
