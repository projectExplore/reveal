
let section =
`<section class="demand-analysis">
    <section>
        <div>
            <h2 class="title">
              原始需求分析
            </h2>
        </div>
    </section>
    <section class="demand-detail">
        <div class="item">
           1、通过文本生成hotfix文件
        </div>
        <div class="item fragment">
           2、通过版本控制工具选择生成hotfix文件
        </div>
        <div class="item fragment">
           3、版本列表可查看该版本的相关信息，
           <div class="ti1-5">如 版本号、提交信息、提交人、补丁信息...</div>
        </div>
        <div class="item fragment">
           4、可用上述两种方式预览补丁内容
           <div class="ti1-5">和下载对应的补丁包（单个下载和批量下载）</div>
        </div>
        <div class="item fragment">
           5、更多相关信息的展示
        </div>
    </section>
    
    <section>
        <div>
            TypeScript React Redux MaterialUI
        </div>
        <div class="fragment">
               Node koa egg
        </div>
    </section>
</section>
`

export default section;
