
/*
*
*   TS 的静态检查
*   React中父子组件传值
*   Redux中全局状态管理
*   运用js新特性，async await 提高代码可读性
*
* */

let section =
    `<section class="project-construct">
    <section>
        <div>
            <h2 class="title">
              总结
            </h2>
        </div>
    </section>
    
    <section>
       <div class="tl pd200">
            <div>TS</div>
            <div>material (css in js)</div>
             <div>Redux中全局状态管理</div>
            <div>js新特性，async await</div>
            <div>React中父子组件传值、状态维护</div>
            <div>react-router 路由匹配 （hash 路由）</div>   
        </div>
    </section>
</section>
`

export default section;
