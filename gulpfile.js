const gulp = require('gulp');
const sass = require('gulp-sass');

gulp.task('default', function() {
    let w1 = gulp.watch('./scss/*.scss');

    w1.on('change', function(e) {
        sass2css(e)
    })
})

function sass2css(e) {
    let oriPath = './scss/*.scss';
    let toPath = './css/'

    gulp.src(oriPath)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(toPath));
}
